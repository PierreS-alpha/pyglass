Pyglass
===============================


Python command line tool to query a GLASS server

**Author:**
Jean-Luc Menut (menut@geoazur.unice.fr)
Eric Marin-Lamellet (marin@geoazur.unice.fr)

**version:** 3.2.1

**updated:** 2024-10-11

# Download


The software can be downloaded at: [https://gitlab.com/gpseurope/pyglass/-/releases](https://gitlab.com/gpseurope/pyglass/-/releases)

## installation

### Python version

* Copy pyglass.py anywhere along with pyglass.ini in any directory
* make pyglass.py executable

## Compiled version

Download the executable for linux and the `.ini` file in the [release page](https://gitlab.com/gpseurope/pyglass/-/releases).

## Requirements for the python version


* Python version 3.10
* a requirement.txt. Note that a recent version of the package `certifi` is needed in order to avoid problems linked to the expiration of the SSL certificate on different platform.

## Compiled version


The procedure is described in the file `nuitka.md` and an executable for linux is provided in the releases.

It's usage is the same than with the python version but the executable is called `pyglass.bin`.

## Usage


A  video demonstration of an older version is available at [https://www.youtube.com/watch?v=hbG8OMzDCBU](https://www.youtube.com/watch?v=hbG8OMzDCBU)


help:
```bash
    $ ./pyglass.py -h
```

Querying station metadata:

```bash
    $ ./pyglass.py stations [CONFIG_OPTIONS] [QUERY_OPTIONS]
```

Querying file metadata

```bash
    $ ./pyglass.py files [CONFIG_OPTIONS] [QUERY_OPTIONS]
```


Pyglass need to know the url of the GLASS-API server to work. Either by passing it as a command line option, or read in the configuration file.


There is currently 2 configuration options::

    -u : the url of a GLASS server (ex for DGW server:  http://gnssdata-epos.oca.eu:8080/GlassFramework)
    -cf : the path to a configuration file
    
If no option configuration are indicate, by default Pyglass use the pyglass.ini configuration file



Examples:
------------

Display the main station metadata in JSON for id AIGL00FRA and AGDE00FRA
```bash
    $ ./pyglass.py stations -m AGDE00FRA AIGL00FRA
```

Download the stations metadata in GeodesyML format for the stations with 9-characters id AIGL00FRA and AGDE00FRA
```bash
    $ ./pyglass.py stations -m AGDE00FRA AIGL00FRA -o GeodesyML
```
Download the stations metadata in IGS site log format for the stations with 9-characters id AIGL00FRA and AGDE00FRA
```bash
    $ ./pyglass.py stations -m AGDE00FRA AIGL00FRA -o SiteLog
```

Display the file metadata for the station having a  9-characters id SOPH00FRA,  in a area defined by the rectangle
  lon >= 5.5, lon <= 7.8,  lat >= 42.1, lat <= 45.6, and using the configuration file `pyglass2.ini`
```bash
 $ ./pyglass.py files -cf pyglass2.ini -m SOPH00FRA -rs  42.1 45.6 5.5 7.8
```


Display the file metadata in JSON for the station with 9-characters id AGDE00FRA for days between  2020-04-01 and 2020-04-10
```bash
    $ ./pyglass.py files -m AGDE00FRA -dds 2020-04-01 -dde 2020-04-10
```

Save the files metadata in a file name RINEX_METADATA_<RUNNING_PYGLASS_DATETIME> 
and download the RINEX files from between 2020-04-01 and 2020-04-10 for AGDE00FRA and store them in the directory ./RINEX
```bash

    $ ./pyglass.py files -m AGDE00FRA -dds 2020-04-01 -dde 2020-04-10 -o download --download-dir ./RINEX
```


For the highrate file:

Display the main station metadata in JSON by bounding box (Minimal latitude, minimal longitude,maximal latitude, maximal longitude)
```bash
    $ ./pyglass.py stations --highrate -ltmin -90 -ltmax 90 -lgmin -180 -lgmax 180
```

Download the stations metadata in IGS site log format for the stations with High Rates files, by bounding box (Minimal latitude, minimal longitude,
maximal latitude, maximal longitude)
```bash
    $ ./pyglass.py stations --highrate -ltmin -90 -ltmax 90 -lgmin -180 -lgmax 180 -o SiteLog
```
Download the stations metadata in GeodesyML format or the stations with High Rates files, by bounding box (Minimal latitude, minimal longitude,
maximal latitude, maximal longitude)
```bash
    $ ./pyglass.py stations --highrate -ltmin -90 -ltmax 90 -lgmin -180 -lgmax 180 -o GeodesyML
```

Save the highrate files metadata in a file name HIGHRATE_RINEX_METADATA_<RUNNING_PYGLASS_DATETIME> 
and download the higrate RINEX files for NOA100GRC and store them in the directory ./HIGHRATE_RINEX
```bash

    $ ./pyglass.py files --highrate -m NOA100GRC -o download --download-dir ./HIGHRATE_RINEX
```



# Use Case: Le Teil 2019 Earthquake


The goal of this use case is to demonstrate how to rapatriate station metadata and RINEX for studying the
Le Teil 2019 Earthquake that happened in november 2019, in the town of Le Teil, Ardèche, France.

First, Let’s find all IGS site logs for the stations located in a radius of 100 km around the epicenter of the earthquake (44.518°N  4.671°E),
installed before 2019 and with data between the 01/11/2019 and the 30/11/2019. The command is
```bash
    $ ./pyglass.py stations -cs 44.518 4.671 100 -idmax 2019-01-01 -dds 2019-11-01 -dde 2019-11-30 -o SiteLog
```
Where the options:

* `stations` allows to search for station metadata.
* `-cs`  selects all station in a circle centered in
 `latitude=44.518` `longitude=4.671` and of radius `radius=100km`
* `-idmax` limits the selection to the station installed before `2019-01-01`
* `-dds` and `-dde` limit the selection to station having data between the two dates in november 2019
* `-o SiteLog` ouputs the stations metadata in IGS Site Log


Now let's download all RINEX for these stations and this period:
```bash
    $ ./pyglass.py files -cs 44.518 4.671 100 -idmax 2019-01-01 -dds 2019-11-01 -dde 2019-11-30 -o download --download-dir RINEX
```

The options are mostly the same, except:
* `stations` is replaced by `files` : that allows to search for files metadata/data
* `SiteLog` is replaced by `download` : that allows to download the RINEX
* `--download-dir` is added: it allows to download the RINEX in a specific directory (here `RINEX`)




