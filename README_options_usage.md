# Usage

help:
```bash
    $ ./pyglass.py -h
```

version:
```bash
    $ ./pyglass.py --version
```


Querying station metadata:

```bash
    $ ./pyglass.py stations [CONFIG_OPTIONS] [QUERY_OPTIONS]
```

Querying file metadata

```bash
    $ ./pyglass.py files [CONFIG_OPTIONS] [QUERY_OPTIONS]
```


## Config options

Pyglass need to know the url of the GLASS-API server to work. Either by passing it as a command line option, or read in the configuration file.

There is currently 2 configuration options::

    -u : the url of a GLASS server
    -cf : the path to a configuration file
    
If no option configuration are indicate, by default Pyglass use the pyglass.ini configuration file for the url of the GLASS server
In the [Default] section, you define the server_default value with the key value corresponding in the [ServerList] section of your choice.
Section [DGW_BACKEND_API]: must not be modified


## Query Options

-m MARKERS [MARKERS ...]                        4 or 9 characters code of the station. The option can take more than one marker and 1 to 9 characters per marker
--highrate                                      Get highrate rinex files when argument target is files (queries only with marker), or get stations with highrate rinex files when argument targert is stations (queries only with ltmin, ltmax, lgmin, lgmax),

-sn SITE_NAME [SITE_NAME ...]                   Station names. The option can take more than one name. If a name has a space it should be quoted.
-rs RECT_SELECT [RECT_SELECT ...]               rectangular selection (order: latitude min, latitude max, longitude min, longitude max)
-cs CIRCLE_SELECT [CIRCLE_SELECT ...]           circular selection (order:latitude, longitude, radius in km)
-ps POLYGON_SELECT [POLYGON_SELECT ...]         polygon selection (order: point A latitude, point A longitude, point B latitude, point B longitude)
-ltmin MIN_LAT                                  Minimal latitude
-ltmax MAX_LAT                                  Maximal latitude
-lgmin MIN_LON                                  Minimal longitude
-lgmax MAX_LON                                  Maximal longitude
-htmin MIN_ALT                                  Minimal height
-htmax MAX_ALT                                  Maximal height
-idmin INSTALLED_DATE_FROM                      Station installed from ...
-idmax INSTALLED_DATE_TO                        Station installed up to ...
-rdmin REMOVED_DATE_FROM                        Station installed from ...
-rdmax REMOVED_DATE_TO                          Station installed up to ...
-nw NETWORK [NETWORK ...]                       network
-inw INVERSE_NETWORKS [INVERSE_NETWORKS ...]    All networks except...
-ag AGENCY [AGENCY ...]                         agency
-ss SATELLITE [SATELLITE ...]                   Satellite System
-rt RECEIVER_TYPE [RECEIVER_TYPE ...]           Receiver type
-at ANTENNA_TYPE [ANTENNA_TYPE ...]             Antenna type
-radt RADOME_TYPE [RADOME_TYPE ...]             Radome type
-co COUNTRY [COUNTRY ...]                       Country
-pv PROVINCE [PROVINCE ...]                     Province/State/Region
-ct CITY [CITY ...]                             City
-dds DATA_DATE_START                            starting date for data
-dde DATA_DATE_END                              end date for data
-pds PUBLISHED_DATE_START                       starting date of publication for data
-pde PUBLISHED_DATE_END                         end date of publication for data
-da DATA_AVAILABILITY                           Data availability (in %)
-sw SAMPLING_WINDOW                             Sampling_window of data (1hr, 30s)
-sf SAMPLING_FREQUENCY                          Sampling frequency for data (1s, 30s)
-ft FILE_TYPE                                   File type (RINEX2, RINEX3)
-moy MINIMUM_OBSERVATION_YEARS                  Maximum amount of years with files
-fs STATUS_FILES                                Status of files (int -3..3)
--download-dir DOWNLOAD_DIR                     Select RINEX download directory
--all                                           Get all metadata in short json
--print-url                                     Only print URL
--timeout TIMEOUT                               Set the timeout for downlading a file. By default: 5s
-nt N, --num-threads N                          The number of threads to use for the download process.
--print-url                                     Only print url query
-o OUTPUT                                       Output Format (cf: GLASS API document). Available short-json, full-json, short-csv, full-csv,full-xml, station-list, rinex-list, download, GeodesyML, SiteLog


Information for the -o OUTPUT option
When querying file metadata, or station and file metadata with highrate option:
Use of short-json or full-json give the same results.
Use of short-csv or full-csv give the same results.

-Information for the --highrate option
Querying file metadata: query available with marker station criteria
Querying station metadata : query available by bounding box criteria (minimal longitude, minimal latitude, maximal longitude, maximal latitude)
