## pyglass_ovs

this is an IPGP-OVS fork of the original _pyglass_ developped at OCA, and distributed under GNU GPL License (according to the original author)
[https://gitlab.com/gpseurope/pyglass](https://gitlab.com/gpseurope/pyglass)

### Maintainer of the original upstream fork
Jean-Luc Menut (menut@geoazur.unice.fr) & Eric Marin-Lamellet (marin@geoazur.unice.fr)

### Maintainer of this fork
Pierre Sakic (sakic@ipgp.fr)

### Main changes w.r.t. upstream
* option `-ie/--ignore_existing` to perform MD5 checksum check & skip __before__ download
* no yes/no confirmation before download
* option `-ql/--quiet_logs` to suppress written logs outputs
