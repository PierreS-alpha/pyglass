# Compilation with nuitka

# Procedure

The code compile well with nuitka (version 1.8.2 was tested)

The command used is :

```bash
python -m nuitka  --show-progress --standalone --follow-imports pyglass.py --no-deployment-flag=self-execution
```

This produce a directory named "pyglass.dist" containing the executable and other files. Only the executable is needed. It is called `pyglass.bin` and is used like the not compiled version.

An executable should be distributed along a [release](https://gitlab.com/gpseurope/indexGD/-/releases/)
## important note

For the moment pyglass is only compiled for linux, and linux with ol libgc don't work. Compiling for other platform has to be investigated.


It is possible to compress the result with UPX (https://upx.github.io/), although I didn't need to do it with nuitka 1.8.2, the executablebeing small enough now.

The command is:

```bash
upx -9  -v pyglass
```

