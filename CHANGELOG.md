CHANGELOG
=========


### 3.2.1 (2024-10-11)
* added:
  * use bump-my-version to manage version
* removed:
  * parasite files toto.csv and test.csv
* changed:
  * always use patch number in versions
  * cleaned output of stations metadata json to include DOI and licence and still be a regular JSON

### 3.2 (2024-08-20)
* added:
  * Nine chars for markers stations
  * Download doi an license for stations
  * check md5 checksum of dowloaded rinex files
  * Manage HR data
  * README for usage options

* changed:
  * correct bug option polygon
  * update README

### 3.1  (2024-03-25)

* added:
  * output for a list of stations
  * correction for the CSV outputs (the CSV provided by the API is not completely valid)
* 
* changed:
  * use the package certifi because of the issues with SSL errors
  * update of the README.md and nuikta.md

### 3.0.1 (2023-11-09)
* changed:
  * correct the bug causing the RINEX to be downloaded in the same directory

### 3.0 (2023-09-27)

* Added:
  * requirements.txt
  * CHANGELOG.md
  * download files in parallel
  * compilation with nuitka (cf nuitka.md)

* Changed:
  * URL creation method
  * README.md

### 2.0 (2022-10-11)
* The branch `python 3`  is now only compatible with `python 3`. Python `3.8+` is supported, but the code should be compatible with previous 3.x versions.
* The branch `python 3` is now the master branch
* requirements.txt file is added
* A timeout for downloading a file is added. By default, the timeout is set at 5s to get a response from the server (not for downloading the file). The --timeout option allows to change its value
* Information on files that failed to be downloaded are now stored in a file `error_<date>.log` in the directory where the command was ran
* Bumping version to 2.0

### 1.1.1 (2022-10-11)
* Added a short documentation for compaling pyglass to a linux binary. no change in version were done

### 1.1.1
* improved documentation

### 1.1.0 (2021-11-10)
*  Passage to python 3 on the branch ` python3` 

### 1.0.9 (2021-11-05)
* README.html replace README.pdf

### 1.0.8 (2021-02-25)
* Improvement of the documentation

### 1.0.4 (2020-12-17)
* IGS SItelogs download corrected

### 1.0.1 (2020-11-18)
* Rework of the documentation and the project directory architecture
* Better handling of the configuration file

### 1.0 (2020-11-18)
* Add Changelog and versioning.

### 0.1.0 (2016-11-15)
* First release on Gitlab.

### 0.1.1 (2017-01-30)

* First version public for the group

### 0.2 (2017-06-29)

* Addition of latitude and longitude options to ease the compatibility with the web client
* Addition of selection by inverse network
* Documentation rewrite


### 0.3 (2017-11-29)

* T2 metadata queries added
